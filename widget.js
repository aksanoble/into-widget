var css = '.modal{display:none}.vanilla-modal .modal{display:block;position:fixed;content:"";top:0;left:0;right:0;bottom:0;background:rgba(0, 0, 0, 0.6);z-index:-1;opacity:0;font-size:0;transition:opacity 0.2s, z-index 0s 0.2s;text-align:center;overflow:hidden;overflow-y:auto;white-space:nowrap;-webkit-overflow-scrolling:touch}.vanilla-modal .modal > *{display:inline-block;white-space:normal;vertical-align:middle;text-align:left}.vanilla-modal .modal:before{display:inline-block;overflow:hidden;width:0;height:100%;vertical-align:middle;content:""}.vanilla-modal.modal-visible .modal{z-index:99;opacity:1;transition:opacity 0.2s}.modal-inner{position:relative;overflow:hidden;max-width:90%;max-height:90%;overflow-x:hidden;overflow-y:auto;background:#fff;z-index:-1;opacity:0;transform:scale(0);transition:opacity 0.2s, transform 0.2s, z-index 0s 0.2s}.modal-visible .modal-inner{z-index:100;opacity:1;transform:scale(1);transition:opacity 0.2s, transform 0.2s}a[rel="modal:close"]{position:absolute;z-index:2;right:0;top:0;width:25px;height:25px;line-height:25px;font-size:13px;cursor:pointer;text-align:center;background:#fff;box-shadow:-1px 1px 2px rgba(0,0,0,0.2)}'
var html = '<div class="modal"><div class="modal-inner"><a rel="modal:close">&times;</a><div class="modal-content"></div></div></div><a href="#modal-3" rel="modal:open"><div class="link-gn-widget foo" style="background-image: none; background-color: #000; display: table; padding:"><img src="assets/img/dummy/2.jpg" alt="logo" width="60" class="link-gn-widget-img" style="padding: 0; display: table-cell; vertical-align: middle; border-radius: 5px; "><div class="" style="display: table-cell; vertical-align: middle; padding-left: 15px"><div >Anna Beuselinck</div><p class="font-sm-x">Organic and biodynamic wine</p><div class="link-gn-widget-cta">Get the Story</div></div><div class="grid-child" style="display: table-cell; vertical-align: middle"> <img src="assets/svg/logo-brand.svg" alt="logo" width="44" class="link-gn-widget-img"></div></div></a><div id="modal-3" style="display:none"><iframe src="http://karenmenezes.com/gn-widget/widget.html" width="360" height="576" frameborder="0"></iframe></div>'
function addStyleString(str, type) {
	var el = document.getElementsByClassName('into-container')[0]
    var node = document.createElement(type);
    node.innerHTML = str;
    el.appendChild(node);
}

addStyleString(css, 'style');
addStyleString(html, 'div');
var VanillaModal = require('vanilla-modal').VanillaModal;
var modal = new VanillaModal();

