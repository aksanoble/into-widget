var path = require('path');

module.exports = {
  entry: './widget.js',
  output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
  },
  devtool: 'source-map',
  externals: {
  	"jquery": "jQuery"
  },
  node: {
  fs: "empty"
}  
};
